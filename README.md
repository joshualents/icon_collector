# Icon Collector

This is an application that will collect the different, free version, icons provided via Font-Awesome and provide them as a file titled 'fa_icons.js'

## My Intentions

There were two major reasons for me to build this project

1. I wanted to have programmatic access to the different icons so that I could use them for other demo projects that i'm working on.

2. I'm beefing up my skills with AWS and am building out projects that I can build and integrate with AWS


## Structure of fa_icons.js

The fa_icons.js file, generated by this project, has the following structure:

```
'use strict';
;(function(input){
    let fa_icons = {
    "500px": {
        "categories": [],
        "icon": "500px",
        "syntax": [
            "fab fa-500px"
        ],
        "types": [
            "brands"
        ]
    },
    "abacus": {
        "categories": [],
        "icon": "abacus",
        "syntax": [
            "fa fa-abacus",
            "fal fa-abacus",
            "fas fa-abacus"
        ],
        "types": [
            "solid"
        ]
    },
    "accessible-icon": {
        "categories": [
            "accessibility",
            "health",
            "users-people",
            "vehicles"
        ],
        "icon": "accessible-icon",
        "syntax": [
            "fab fa-accessible-icon"
        ],
        "types": [
            "brands"
        ]
    } ...

    input.fa_icons = fa_icons;
}).call(this);
```

Where each body has the following four fields:

### categories

This will always be an array, but it may be empty. It may also have one or more strings in it, representing each category that icon is associated with

### icon

This is the name of the icon. It's the same value that is in the key. The reason I included it in here is so the json-value (category, icon, syntax, types) can stand on it's own.

It doesn't really serve much purpose, but if you were using it programmatically, you might like having it there in some use-cases.

### syntax

This is an array containing strings, where each string contained is a valid syntax for representing the icon in the browser. I've found that the brands do not render with the other non-`fab` leading classes.

Now, I couldn't easily tell which icons were technically 'light' icons. I'm taking the assumption that all 'solid' icons have a corresponding 'light' icon. I have not confirmed this.

The 'regular' icons are clearly identified and it seems that not every 'solid' icon has a 'regular', but every 'regular' seems to have a 'solid'... I could be wrong though.

What I'm getting at is that my current stance is that none of the sytax I've provided should be violating. I'm just not always sure that all versions of the syntax will generate a slightly different icon (i.e. some of the different syntaxes for the same icon might result in the same exact icon). My main goal was to generate non-breaking syntax for programmatic icon generation and I feel i've done that here.

### types

This is an array indicating what types the icon is/has.

The current options are:

* brands
* solid
* regular

'light' should also be a type, but it's not explicitly listed, which has lead me to assume it's a sister-type to something like 'solid'

## Python Version

This project was written from the perspecitive of Python 3.6

## The Different Ways to Run the Application

On top of that use-case of using programmatic access of Font-Awesome icons as a tool for other demos, my other purpose it to utilize AWS with this project.

I'm setting up 2 ways to run this application:

1. locally on your machine, like a normal python project
2. utilizing AWS to build it yourself


## Environment Variables

The first three environment variables are only going to matter if you are planning on utilizing AWS and outputting to S3. If you are not, then you don't need them.

If you do plan on utilizing AWS, the envinronment variables will be set through your interaction with the cloudformation template.

Here are the three environment variables:

1. OUTPUT_TO_S3

    > set to true if you plan on utilizing AWS and sending to S3

2. BUCKET_NAME

    > the name of the bucket you intend to PUT the file to

3. BUCKET_KEY

    > the destination on the bucket that you plan to put the file to

4. PRINT_PRETTY

    > set to true if you want the output to have spacing/indention. It will almost double the size of the output file if you have it on, but it is more readable.


## External Dependencies

I utilize 3 dependencies to accomplish my goal of parsing the project. I conditionally utilize a 4th dependency for interacting with AWS.

1. requests

    > used to retrieve the Font-Awesome files from Github

2. PyYaml

    > used to parse one of the yaml files from the Font-Awesome repo on Github

3. BeautifulSoup4

    > used to parse the HTML scraped from the Github page.
    > I was originally cloning the project locally and parsing through it there, but I need minimal files and this approach will save the Lambda function some overhead

4. boto3

    > this dependency is only needed if you plan on running the application locally && you want the application to upload the file to S3.
    > otherwise, you do not need it.
    > if you end up making a lambda function, by using the CloudFormation template, you still won't need to install this (Lambda already has this library on hand)
    > again, you only need this dependency for the specific case of running on your machine, but wanting to output to S3

## Installing the External Dependencies

The primary dependencies (requests, PyYaml, and BeautifulSoup4) are listed in the requirements.txt file of this project.

If you want to install the project:

```
pip install -r requirements.txt
```

Now, for that edge case of wanting to run locally, but deploy the output to S3. You will need boto3 to be installed as well:

```
pip install boto3
```

## Running the project locally

too easy. Just execute the \_\_init\_\_.py file, in the icon_collector directory, and it will do the rest.

> Wherever you run it from, it will output an "fa_icons.js" file to that directory

for example, if you were in the top directory in this project, you would run the following:

```
python3 icon_collector/__init__.py
```

That will complete in a couple seconds and you will have your outputted file (fa_icons.js)


## AWS Utilization

My objective is to get deep with AWS on this project. I'm shooting for the following setup:

1. Use CloudFormation to setup my infrastructure, which will perform the rest of the steps for me in a programmatic way

    > I want to parameterize it as much as possible so that others can use it too and learn from it with me

2. Deploy the icon_collector function as a Lambda function on AWS

3. Let the Lambda function output the fa_icons.js file to an S3 bucket

4. Make the fa_icons.js file available on CloudFront, utilizing CloudFront as a CDN

5. Create an API Gateway that points to my CloudFront distribution

6. Have Route53 generate a record that can resolve for people to hit a clean route to get the fa_icons.js file







