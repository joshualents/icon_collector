import unittest
from unittest import mock

import icon_collector as ic

def mock_get_request(the_return_string):
    '''
    wrapper to generated mock requests
    '''
    def the_mocked_request(x=None, y=None):
        '''
        function to simulate a requests.get() function call
        '''
        class MockResponse:
            @property
            def text(self):
                return the_return_string
        return MockResponse()

    return the_mocked_request

def mock_BeautifulSoup(the_return_array):
    def the_mocked_soup(x=None, y=None):
        class MockedSoup:
            def __init__(self, a, b):
                pass

            def find(self, blah=None):
                return MockedSoup(x, y)

            def find_all(self, tag=None, attrs=None):

                class Record:
                    def __init__(self, record):
                        self.record = record

                    @property
                    def attrs(self):
                        if tag == 'a':
                            return {'title': 'foo.svg'}
                        else:
                            return self.record

                for record in the_return_array:
                    yield Record(record)

        return MockedSoup(x, y)
    return the_mocked_soup

def mock_yaml(the_return_value):
    def the_mocked_yaml(something=None):
        '''
        function to simulate a requests.get() function call
        '''
        return {'some_category': {'icons': ['foo']}}

    return the_mocked_yaml

class TestIconCollector(unittest.TestCase):

    def test_build_http_path(self):
        expected = 'http://powpow.com/some/route/some_file.txt'
        elements = ['http://powpow.com', 'some/route/', 'some_file.txt']
        output = ic._build_http_path(elements)
        self.assertEqual(output, expected)

    @mock.patch('requests.get', side_effect=mock_get_request('pow'))
    @mock.patch('icon_collector.BeautifulSoup', side_effect=mock_BeautifulSoup([{'title': 'brands', 'href': '/brands/'},
                                                                                {'title': 'solid', 'href': '/solid/'}]))
    def test_collect_icon_types(self, mock_get, mock_soup):
        icons = ic.collect_icon_types()
        self.assertTrue('foo' in icons)
        self.assertTrue('brands' in icons['foo']['types'])
        self.assertTrue('solid' in icons['foo']['types'])
        self.assertEqual('foo', icons['foo']['icon'])

    @mock.patch('yaml.load', side_effect=mock_yaml('pow'))
    def test_collect_icon_categories(self, mock_yaml):
        icons = {'foo': {'icon': 'foo', 'types': ['brands', 'solid'], 'syntax': [], 'categories': []}}
        ic.collect_icon_categories(icons)
        self.assertTrue('some_category' in icons['foo']['categories'])

    def test_collect_icon_syntax(self):
        icons = {'foo': {'icon': 'foo', 'types': ['brands', 'solid'], 'syntax': [], 'categories': []}}
        ic.collect_icon_syntax(icons)
        expected = {'fa fa-foo', 'fal fa-foo', 'fas fa-foo', 'fab fa-foo'}

        self.assertEqual(expected, set(icons['foo']['syntax']))




if __name__ == '__main__':
    unittest.main()
