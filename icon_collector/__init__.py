'''
This project is designed to collect specific filenames and files in order to build a
programmatic list of the free Font-Awesome icons, for programmatic access.

It will generate a file, fa_icons.js

'''
import json
import os
import re

from timeit import default_timer as timer

import yaml
import requests
from bs4 import BeautifulSoup


GITHUB = 'https://github.com/'
OUTPUT_TO_S3 = os.environ.get('OUTPUT_TO_S3')
PRINT_PRETTY = os.environ.get('PRINT_PRETTY')

if os.environ.get('OUTPUT_TO_S3'):
    # conditionally import boto3 for outputting the file to AWS S3
    import boto3
    s3_client            = boto3.client('s3')


def _build_http_path(elements):
    '''
    A helper function to build http paths.
    I built this because the filepaths, to the icons stored by type, are relative.
    So, I needed to build a full path.

    :params: elements: list of strings that will be formed together to build a URL
    :output: a string representing a URL
    '''
    if not elements:
        return ''
    elements = '/'.join(elements)
    elements = elements.split('//', 1)

    if not len(elements) == 2:
        raise ValueError("Could not build http path properly")

    pattern = re.compile(r'\/+')
    elements[1] = re.sub(pattern, '/', elements[1])

    return '//'.join(elements)


def collect_icon_types():
    '''
    This function will start the collection of icons. It will lookup the icons, stored by their type,
    then add them to the dictionary to be returned along with the types they have.

    sample of the returned dictionary:
        {'500px': {'icon': '500px', 'syntax': [], 'categories': [], 'types': ['brands']}}

    :params: N/A
    :output: a dictionary storing the icons and the types they have
    '''
    RAW_SVG_DIRECTORY = _build_http_path([GITHUB, '/FortAwesome/Font-Awesome/tree/master/', 'svgs'])
    icons   = {}
    icon_types = {}
    try:
        # retrieve the RAW-SVG directory from the font-awesome github,
        soup = BeautifulSoup(requests.get(RAW_SVG_DIRECTORY).text, 'html.parser')
        tbody = soup.find('tbody')
    except Exception as e:
        message = "Failure on retreiving the names of the types, from the raw-svg directory on the Font-Awesome Github repo: "
        print(message)
        raise ValueError(message + str(e))

    # get the links that belong to the body of the table
    links = tbody.find_all(attrs={'class': 'js-navigation-open'})

    if not links:
        message = "The table for raw-svg isn't what we thought it was... bailing"
        print(message)
        raise ValueError(message)

    for link in links:
        text = link.attrs.get('title') # name of the type
        href = link.attrs.get('href')  # relative url directory containing icons of the type specified in the previous line

        if not text and not href:
            message = 'No href or no name was present for raw-svg link: {}'.format(link)
            raise ValueError(message)

        # storing the type and the href where that type is located
        icon_types[text] = href

    for cur_type in icon_types.keys():
        try:
            # retrieve the html file containing the list of all icons for the current type
            href      = _build_http_path([GITHUB, icon_types.get(cur_type)])
            type_page = requests.get(href).text
            soup      = BeautifulSoup(type_page, 'html.parser')
            tbody     = soup.find('tbody')
        except Exception as e:
            message = 'failure on retrieving the types for "{}" from href: {}'.format(cur_type, href)
            print(message)
            raise ValueError(message + str(e))

        icon_types[cur_type] = {}
        # for each icon file, remove the '.svg' from it and store it as an icon
        for row in tbody.find_all('a', attrs={'class': 'js-navigation-open'}):
            icon = row.attrs.get('title')[:-4] # removing the trailing .svg
            if icon not in icons:
                icons[icon] = {'icon': icon, 'syntax': [], 'categories': [], 'types': []}
            icons[icon]['types'].append(cur_type)

    return icons


def collect_icon_categories(icons):
    '''
    This function will add all icons with their category, if the icon already exists, then it will be updated with the found categories.

    This is a side-effecting function that will update the icons dictionary.

    sample of the mutated dictionary:
        {'amazon-pay': {'icon': 'amazon-pay', 'syntax': [], 'categories': ['payments-shopping'], 'types': ['brands']}}

    :params: icons: a dictionary containing the icons already discovered
    :output: N/A
    '''
    categories_file = 'https://raw.githubusercontent.com/FortAwesome/Font-Awesome/master/metadata/categories.yml'
    try:
        # retrieve the file and load it as yaml for parsing
        categories = requests.get(categories_file).text
        categories = yaml.safe_load(categories)
    except Exception as e:
        message = 'failed to get the categories.yml file. {}'.format(e)
        raise ValueError(message)

    for category in categories.keys():
        for icon_name in categories[category]['icons']:
            if icon_name not in icons:
                icons[icon_name] = {'icon': icon_name, 'types': [], 'syntax': [], 'categories': []}

            # side-effecting function
            icons[icon_name]['categories'].append(category)


def collect_icon_syntax(icons):
    '''
    This function will update the acceptable syntax for each icon in the icons dictionary.

    This is a side-effecting function that will alter the icons dictionary.

    {'wine-glass': {'icon': 'wine-glass',
                    'types': ['solid'],
                    'categories': [],
                    'sytnax': ['fa fa-wine-glass',
                               'fas fa-wine-glass',
                               'fal fa-wine-glass']}}

    :params: icons: a dictionary containing the icons already discovered
    :output: N/A
    '''
    for icon_name in icons.keys():
        leader_keys = []
        for icon_type in icons[icon_name]['types']:
            if icon_type == 'brands':
                leader_keys += ['fab']
            elif icon_type == 'regular':
                leader_keys += ['far']
            elif icon_type == 'solid':
                leader_keys += ['fa', 'fal', 'fas']
            else:
                raise ValueError("New icon type that doesn't have a proper leader key: {}".format(icon_type))

        for leader in leader_keys:
            icons[icon_name]['syntax'].append('{} fa-{}'.format(leader, icon_name))


def lambda_handler(event=None, context=None):
    '''
    This is the main function that will be hit.
    The signature lines up with what AWS Lambda is expecting to see,
    which is a function that accepts 2 arguments.

    The arguments aren't needed, as this is not an interactive project.
    It's a task oriented project designed to collect the icons

    :params event: the event object that Lambda will pass to the function (serves no purpose in this project)
    :params context: the context that Lambda will pass to the function (serves no purpose in this project)

    :returns: N/A
    '''
    _main()


def upload_file_to_s3(body):
    '''
    Function used to upload the file (a string) to AWS S3.

    It takes the argument, a string, and PUTS it to a key in an AWS S3 Bucket.

    :params: body: the string that will be PUT to S3 (the fa_icons.js file content)
    :returns: N/A
    '''
    s3_client.put_object(Bucket=os.environ.get('BUCKET_NAME'),
                         Key=os.environ.get('BUCKET_KEY'),
                         Body=body,
                         ACL='public-read')

def build_output(icons):
    '''
    Formats the assembled dictionary, containing all the desired info about the Font-Awesome icons, into
    a file that can be utilized in a browser.
    Here is a sample structure of the output:
        'use strict';
        ;(function(input){
            let fa_icons = {'500px': {'categories': [], 'icon': 500px', 'types': ['brands'], 'syntax': ['fab fa-500px']}};
            input.fa_icons = fa_icons;
        }).call(this);

    :params: icons: dictionary containing the assembled information regarding each icon
    :returns: string representing the icons dictionary stored as a variable inside an IIFE
    '''
    return json.dumps(icons, indent=4, sort_keys=True)
    return ''''use strict';
;(function(input){
    let fa_icons = %s;
    input.fa_icons = fa_icons;
})(this);''' % (json.dumps(icons, indent=4, sort_keys=True) if PRINT_PRETTY else json.dumps(icons, sort_keys=True))


def _main():
    '''
    The true 'main' function. It will kick off the retrieval off all needed files from the Font-Awseome Github page,
    then assemble the icons,
    then output the fa_icons.js file
    '''
    start = timer()
    icons = collect_icon_types()
    collect_icon_categories(icons)
    collect_icon_syntax(icons)

    end    = timer()
    output = build_output(icons)

    time_diff = 'Completed in {} seconds'.format(end-start)

    # if you specify the 3 environment variables and follow the rest of the instructions,
    # listed in the README, you can output this thing to your S3 bucket
    if OUTPUT_TO_S3:
        upload_file_to_s3(output, time_diff)
    else:
        with open('fa_icons.js', 'w') as f:
            f.write(output)
        print(time_diff)


if __name__ == '__main__':
    lambda_handler(None, None)
