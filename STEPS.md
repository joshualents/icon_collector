# Building the SAM template to deploy this application

> The following commands are intended to be ran from inside the project directory. (not so much steps 1 & 2, but the rest)
> You should be in the directory that has the README.md file, as well as that icon_collector directory containing the __init__.py file

1. **install aws cli** (https://docs.aws.amazon.com/cli/latest/userguide/installing.html)
2. **make the build directory**

    ```
    mkdir build
    ```

3. **install dependencies to the build directory**

    ```
    pip install -r requirements.txt -t build
    ```

4. **copy the project files to the build directory**

    ```
    cp icon_collector/*.py build
    ```






